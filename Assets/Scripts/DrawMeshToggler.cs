﻿using UnityEngine;
using System.Collections;

public class DrawMeshToggler : MonoBehaviour {

	Renderer _renderer;

	void Start(){
		_renderer = GetComponent<Renderer> ();
	}
		
	// called by UIGestureManager when user performs a Select gesture
	void OnSelect() {
		Debug.Log ("on select in draw mesh toggler");

		SpatialMapping.Instance.DrawVisualMeshes = !SpatialMapping.Instance.DrawVisualMeshes;
	}
}
