﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineAnnotation {

	Vector3[] linePoints;
	public GameObject lineObject;
	LineRenderer lineRenderer;

	public LineAnnotation() {
		linePoints = new Vector3[0];


		lineObject = new GameObject ("Line");
		lineRenderer = lineObject.AddComponent<LineRenderer> () as LineRenderer;
		lineRenderer.SetWidth (0.0025f, 0.0025f);
		lineRenderer.material = (Material)Resources.Load ("LineMaterial", typeof(Material));
		lineRenderer.SetColors (Color.white, Color.white);

		int numLinePoints = 0;
		lineRenderer.SetVertexCount (numLinePoints);
		lineRenderer.SetPositions (linePoints);

		UpdateRenderer ();
	}

	void UpdateRenderer() {
		lineRenderer.SetVertexCount (linePoints.Length);
		lineRenderer.SetPositions (linePoints);
	}

	public void AddPoint(Vector3 point) {
		System.Array.Resize<Vector3> (ref linePoints, linePoints.Length + 1);
		linePoints [linePoints.Length - 1] = point;
		UpdateRenderer ();
	}
}
