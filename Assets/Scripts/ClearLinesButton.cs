﻿using UnityEngine;
using System.Collections;

public class ClearLinesButton : MonoBehaviour {

	// called by UIGestureManager when user performs a Select gesture
	void OnSelect() {
		Debug.Log ("on select in clear lines button");

		LineAnnotationManager.Instance.ClearLines ();
	}
}
