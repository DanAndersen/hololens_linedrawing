﻿using UnityEngine;
using System.Collections;

public class UIStateManager : MonoBehaviour {

	public static UIStateManager Instance { get; private set; }

	public enum UIState {Normal, DrawingLine};

	public UIState state;

	void Awake() {
		state = UIState.Normal;
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
