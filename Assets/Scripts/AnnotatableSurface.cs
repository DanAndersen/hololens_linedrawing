﻿using UnityEngine;
using System.Collections;

public class AnnotatableSurface : MonoBehaviour {

	// Called by the UIGestureManager
	void OnSelect() {

		if (UIStateManager.Instance.state == UIStateManager.UIState.DrawingLine) {
			if (LineAnnotationManager.Instance.currentLine != null) {
				GameObject focusedPoint = HoloToolkit.Unity.GazeManager.Instance.FocusedObject;
				if (focusedPoint != null) {
					Vector3 hitPoint = HoloToolkit.Unity.GazeManager.Instance.Position;
					LineAnnotationManager.Instance.currentLine.AddPoint (hitPoint);
				}
			}
		}
	}
}
