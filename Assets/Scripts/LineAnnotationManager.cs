﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineAnnotationManager : MonoBehaviour {

	public static LineAnnotationManager Instance { get; private set; }

	List<LineAnnotation> lines;
	public LineAnnotation currentLine;

	// Use this for initialization
	void Start () {

		Instance = this;

		lines = new List<LineAnnotation> ();

		/*
        for (int i = 0; i < 10; i++)
        {
			Line line = CreateNewLine ();

			for (int j = 0; j < 15; j++) {
				line.AddPoint (new Vector3 (Random.Range (-1f, 1f), Random.Range (-1f, 1f), Random.Range (-1f, 1f)));
			}

			FinishCurrentLine ();
        }
        */
	}

	public void ClearLines() {
		foreach (LineAnnotation line in lines) {
			Destroy (line.lineObject);
		}
		lines.Clear ();
	}

	public LineAnnotation CreateNewLine() {
		LineAnnotation line = new LineAnnotation ();
		lines.Add (line);
		line.lineObject.transform.parent = transform;
		currentLine = line;
		return line;
	}

	public void FinishCurrentLine() {
		currentLine = null;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
