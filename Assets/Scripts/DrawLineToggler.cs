﻿using UnityEngine;
using System.Collections;

public class DrawLineToggler : MonoBehaviour {

	Renderer _renderer;

	void Start(){
		_renderer = GetComponent<Renderer> ();
		UpdateButton ();
	}

	void UpdateButton() {
		if (UIStateManager.Instance.state == UIStateManager.UIState.DrawingLine) {
			_renderer.material.color = Color.green;
		} else {
			_renderer.material.color = Color.red;
		}
	}

	// called by UIGestureManager when user performs a Select gesture
	void OnSelect() {
		Debug.Log ("on select in draw line toggler");

		if (UIStateManager.Instance.state != UIStateManager.UIState.DrawingLine) {
			UIStateManager.Instance.state = UIStateManager.UIState.DrawingLine;
			LineAnnotationManager.Instance.CreateNewLine ();
		} else {
			UIStateManager.Instance.state = UIStateManager.UIState.Normal;
			LineAnnotationManager.Instance.FinishCurrentLine ();
		}

		UpdateButton ();
	}
}
