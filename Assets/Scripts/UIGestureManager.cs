﻿using UnityEngine;
using UnityEngine.VR.WSA.Input;

public class UIGestureManager : MonoBehaviour
{
	public static UIGestureManager Instance { get; private set; }

	public GestureRecognizer recognizer { get; private set; }

	// Use this for initialization
	void Start()
	{
		Instance = this;

		// Set up a GestureRecognizer to detect Select gestures.
		recognizer = new GestureRecognizer();
		recognizer.TappedEvent += (source, tapCount, ray) =>
		{
			GameObject focusedObject = HoloToolkit.Unity.GazeManager.Instance.FocusedObject;
			// Send an OnSelect message to the focused object and its ancestors.
			if (focusedObject != null)
			{
				focusedObject.SendMessageUpwards("OnSelect");
			}
		};
		recognizer.StartCapturingGestures();
	}

	// Update is called once per frame
	void Update()
	{
		
	}


}